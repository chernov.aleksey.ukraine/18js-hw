const user1 = {
    name: 'Ivan',
    lastName: 'Petrov',
    place: 'Dnipro',
    age: 45,
    wife: {
        name: 'Svetlana', age: 41, place: 'Dnipro', assets: {
            prop: {
                name: 'building',
                sqr_meters: {
                    Floor1: 150,
                    Floor2: 150,
                    Floor3: 0,
                    Floor4: 0,
                },
            },
            estimCost$: 250000,
        },
    },
    parents: {
    mother: { name: 'Irina', place: 'Krivy Rih', age: 63, spcial: [12, 15, 22], },
    father: { name: 'Valeriy', place: 'Krivy Rih', age: 72,}
  },
  arr: ['one', 'two', 'three', 'four', ['again one', obj = {login: 'xyz@abc', code: 'no any'}, 'again two', ['next 1', 'next 2'],],],
}

function absoluteClone(start) {
  const finish = {};
  for (const key in start) {
    if ( Array.isArray (start[key])  ) {
     
      finish [key] = start[key].flat(0);
     } else if (typeof start[key] === 'object') {
      finish[key] = absoluteClone(start[key]);
    } else  {
      finish[key] = start[key]
    }
  }
  return finish;
}
const user2 = absoluteClone(user1);
console.log(user2);
user1.arr = ['the one', ' the two', 'three', 'four', ['again little one', obj = {login: 'uvwxyz@abc', code: 'no any'}, 'again two', ['next 1', 'next 2'],],];
console.log(user1);


